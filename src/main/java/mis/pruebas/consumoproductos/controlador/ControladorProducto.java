package mis.pruebas.consumoproductos.controlador;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import mis.pruebas.consumoproductos.modelo.Producto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping("/api-productos-puente/v1/productos")
public class ControladorProducto {

    @Value("${api.productos.v1}")
    String urlProductos;

    @GetMapping
    public ResponseEntity<List<Producto>> obtenerListaProductos() {
        final RestTemplate plantilla = new RestTemplate();
        final ResponseEntity<String> respuesta = plantilla.getForEntity(this.urlProductos, String.class);

        System.out.println(respuesta.getBody());

        if(respuesta.getStatusCode().isError()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        try {
            final ObjectMapper mapper = new ObjectMapper();
            final JsonNode nodo = mapper.readTree(respuesta.getBody());
            final List<Producto> pp = new ArrayList<Producto>();
            final Iterator<JsonNode> it = nodo.get("_embedded").get("productoList").elements();
            while(it.hasNext()) {
                final JsonNode j = it.next();
                final Producto p = new Producto();
                p.nombre = j.get("nombre").asText();
                p.precio = j.get("precio").asDouble();
                pp.add(p);
            }

            return ResponseEntity.ok(pp);
        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY);
        }
    }

    @GetMapping("/{idProducto}")
    public ResponseEntity<Producto> obtenerProducto(@PathVariable String idProducto) {
        final RestTemplate plantilla = new RestTemplate();
        final Producto p = plantilla.getForObject(this.urlProductos.concat("/").concat(idProducto), Producto.class);
        return ResponseEntity.ok(p);
    }
}
